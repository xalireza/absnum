import java.util.Objects;
import java.util.Scanner;

public class abcnum
{
    //defining required variables and constants
    public static boolean log=false;
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String YELLOW = "\u001B[33m";
    public static final String GREEN = "\u001B[32m";
    public static final String BLUE = "\u001B[34m";
    public static final String RED = "\u001B[31m";
    public static final String MAIN_PRINT_COLOUR = GREEN;
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args)
    {
        //defining main function of program which program starts from
        print("Hello");
        print("Welcome to abs calculator");
        {
            mainMenu();
        }
    }
    public static void mainMenu()//generates a semi ui menu to interact with user
    {
        while (true)//mainloop such as gui mainloop keeps the user in menu
        {
            print("Choose The function : ");
            print("S : Start app");
            print("C : Close app","error");
            print(">>>",true);
            String inp = scanner.nextLine();//scans for user input
            //if statement checks the user input and controls bad input to keep program avoid crashes
            if (Objects.equals(inp, "S") | Objects.equals(inp, "s"))
            {
                Calc();
            }
            else if (Objects.equals(inp, "C") || Objects.equals(inp, "c"))
            {
                clear();
                print("Hope you Enjoy!","coloured");
                System.exit(0);//lets use exit
            }
            else
            {
                print("Bad input :(","error");
                print("Try again");
            }
        }
    }
    public static void Calc()//calculate the abs of user number
    {
        while (true)
        {
            print("Inter your number or write q to return to main menu","coloured");
            print(">>>",true);
            String input =scanner.nextLine();
            if (Objects.equals(input, "q") | Objects.equals(input, "Q"))
                mainMenu();
            else
            {
                try//Exception handling for string inputs
                {
                    int a = Integer.parseInt(input);
                    print(Integer.toString(Myabs(a)));
                }
                catch (Exception e)
                {
                    print("Bad input :(","error");
                    print("Try again");
                }
            }
        }
    }
    public static int Myabs(int number)//main calculation function
    {
        if (log)
            print("calculating");

        if (number >=0)
            return number;
        else
            return (-1*number);
    }
    public static void clear()//clears the terminal
    {
        print("\033[H\033[2J");
    }
    public static void print(String string)//customized print function instead of System.out.println
    {
        System.out.print(MAIN_PRINT_COLOUR+string+ANSI_RESET+"\n");
    }
    public static void print(String string,boolean noNewLine)//print function overwrite for no newline functionality
    {
        if (noNewLine)
            System.out.println(GREEN+string+ANSI_RESET);
        else
            print(string);
    }
    public static void print(String string,String type)//overwrite print function for coloured printing
    {
        switch (type)
        {
            case "error" :
                System.out.print(RED + string + ANSI_RESET + "\n");
                break;
            case "attention" :
                System.out.print(YELLOW + string + ANSI_RESET + "\n");
                break;
            case "coloured" :
                System.out.print(BLUE + string + ANSI_RESET + "\n");
                break;
            default :
                System.out.print(GREEN + string + ANSI_RESET + "\n");
                break;
        }


    }
}
